#!/bin/bash

DOCKER_IMAGE=daju/of:0.11.0
PROJECT_ROOT=${PWD}

DISPLAY=$(env | grep DISPLAY= | sed 's/DISPLAY=//')
export DISPLAY=$DISPLAY

docker run -it --rm -v ${PROJECT_ROOT}:/tmp/work \
    -u $(id -u):$(id -g) \
    -e DISPLAY=$DISPLAY --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    ${DOCKER_IMAGE} /bin/bash


